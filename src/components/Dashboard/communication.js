import React from "react";
import { Link } from "react-router-dom";
export default function communication() {
  return (
    <div className="dashboard communication">
      <div className="courses-section">
        <div className="col-md-12 col-lg-12">
          <div className="setting">
            <div className="setting-img">
              <a href="profile.html">
                <img src="../imgs/s3.jpg" alt="Img" />
              </a>
            </div>
            <h5>
              Teacher
              <i className="far fa-bell" />
            </h5>
          </div>
        </div>
        <div className="container">
          <div className="communicate-content">
            <nav
              className="nav nav-pills nav-justified"
              id="nav-tab"
              role="tablist"
            >
              <a
                aria-controls="nav-home"
                aria-selected="true"
                className="nav-item nav-link active"
                data-toggle="tab"
                href="#nav-home"
                id="nav-home-tab"
                role="tab"
              >
                Messages
              </a>
            </nav>
            <div className="tab-content" id="nav-tabContent">
              <div
                aria-labelledby="nav-home-tab"
                className="tab-pane fade show active"
                id="nav-home"
                role="tabpanel"
              >
                <div className="messages">Messages</div>
              </div>
            </div>
          </div>
          {/*Front Page of Dashboard*/}
        </div>
      </div>
    </div>
  );
}
