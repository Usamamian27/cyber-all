import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class Layout extends Component {
  render() {
    const Header = () => {
      return (
        <header className="dashboard-header">
          <nav className="navbar navbar-dark bg-dark">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarToggleExternalContent"
              aria-controls="navbarToggleExternalContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
              id="custom-hide"
            >
              <span className="navbar-toggler-icon" />
            </button>
          </nav>
          <ul className="collapse" id="navbarToggleExternalContent">
            <Link
              className="navbar-brand blue"
              style={{ marginLeft: 40 }}
              to="/"
            >
              <img src="../imgs/Logo1.png" alt="Brand" />
            </Link>
            <li>
              <Link to="/teacher/add-courses">
                <i className="fas fa-video" />
                Courses
              </Link>
            </li>
            <li>
              <Link to="/teacher/communication">
                <i className="fas fa-envelope" />
                Communication
              </Link>
            </li>
            <li>
              <Link to="/teacher/performance">
                <i className="fas fa-chart-bar" />
                Performance
              </Link>
            </li>
            <li>
              <Link to="#!">
                <i className="fas fa-question-circle" />
                Resources
              </Link>
            </li>
            <li>
              <Link to="/">
                <i className="fas fa-sign-out-alt" />
                Logout
              </Link>
            </li>
          </ul>
        </header>
      );
    };

    const Footer = () => {
      return (
        <footer className="footer dashboard-footer">
          <div className="container">
            <div className="col-md-12 col-lg-12">
              <div className="row">
                <div className="col-md-2 col-lg-2 col-sm-12">
                  <div className="footer-logo-img">
                    <img src="../imgs/logo1.png" alt="Brand" />
                  </div>
                </div>
                <div className="col-md-8 col-lg-8 col-sm-12 center">
                  <div className="footer-content">
                    <ul>
                      <li>© Logo 2019</li>
                      <li>
                        <a href="#">Jobs</a>
                      </li>
                      <li>
                        <a href="#">Privacy Policy</a>
                      </li>
                      <li>
                        <a href="#">Term of Use</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-md-2 col-lg-2 col-sm-12">
                  <div className="footer-content social-icon">
                    <ul>
                      <li>
                        <a href="#">
                          <i className="fab fa-twitter" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fab fa-instagram" />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="fab fa-facebook" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer>
      );
    };
    return (
      <div>
        <Header />
        {this.props.children}
        <Footer />
      </div>
    );
  }
}
