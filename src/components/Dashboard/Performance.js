import React from "react";
import { Link } from "react-router-dom";
export default function Performance() {
  return (
    <div className="dashboard performance">
      <div className="courses-section">
        <div className="col-md-12 col-lg-12">
          <div className="setting">
            <div className="setting-img">
              <a href="profile.html">
                <img src="../imgs/s3.jpg" alt="Img" />
              </a>
            </div>
            <h5>
              Teacher
              <i className="far fa-bell" />
            </h5>
          </div>
        </div>
        <div className="container">
          <div className="perform-content">
            <h3>Overview</h3>
            <p>Get top insights about your performance</p>
            <div className="chart">
              <canvas id="c" />
              <div className="label">text</div>
            </div>
            <a href="#" className="btn-become bg-blue">
              Revenue Report
            </a>
          </div>
          {/*Front Page of Dashboard*/}
        </div>
      </div>
      {/*Middle Section Ends Here*/}
    </div>
  );
}
