import React from "react";
import { connect } from "react-redux";
import { INCREMENT_VALUE, DECREMENT_VALUE } from "../utils/types";
import { useEffect } from "react";

function Calc(props) {
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${props.counter.myValue} times`;
  });
  const { myValue } = props.counter;
  const onIncrement = () => {
    props.incrementService();
  };
  const onDecrement = () => {
    props.decrementService();
  };
  return (
    <div>
      <div className="container">
        <div className="row m-5">
          <div
            className="col-md-8"
            style={{
              backgroundColor: myValue > 0 ? "green" : "red",
              color: "white",
              padding: 26,
              fontSize: 26
            }}
          >
            {myValue}
          </div>
          <div className="col-md-4">
            <div className="btn btn-success btn-block" onClick={onIncrement}>
              +
            </div>{" "}
            <div className="btn btn-danger btn-block" onClick={onDecrement}>
              -
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  counter: state.counter
});
const mapDispatchToProps = dispatch => ({
  incrementService: () => {
    dispatch({
      type: INCREMENT_VALUE,
      payload: 1
    });
  },
  decrementService: () => {
    dispatch({
      type: DECREMENT_VALUE,
      payload: 1
    });
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Calc);
