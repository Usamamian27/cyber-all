import React from "react";
import { Link } from "react-router-dom";
function footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="col-md-12 col-lg-12">
          <div className="row">
            <div className="col-md-2 col-lg-2 col-sm-12">
              <div className="footer-logo-img">
                <img src="imgs/logo1.png" alt="Brand" />
              </div>
            </div>
            <div className="col-md-8 col-lg-8 col-sm-12 center">
              <div className="footer-content">
                <ul>
                  <li>© Logo 2019</li>
                  <li>
                    <Link to="#!">Jobs</Link>
                  </li>
                  <li>
                    <Link to="#!">Privacy Policy</Link>
                  </li>
                  <li>
                    <Link to="#!">Term of Use</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-2 col-lg-2 col-sm-12">
              <div className="footer-content social-icon">
                <ul>
                  <li>
                    <Link to="#!">
                      <i className="fab fa-twitter" />
                    </Link>
                  </li>
                  <li>
                    <Link to="#!">
                      <i className="fab fa-instagram" />
                    </Link>
                  </li>
                  <li>
                    <Link to="#!">
                      <i className="fab fa-facebook" />
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default footer;
