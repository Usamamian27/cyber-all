import React from "react";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/layout/header";
import Footer from "./components/layout/footer";
import landing from "./components/layout/landing";
import Grid from "./components/courses/grid";
import CourseDetail from "./components/courses/courseDetail";
import Enroll from "./components/courses/enroll";
import TeachStep1 from "./components/teacher/teachStep1";
// teacher
import Teach from "./components/teacher/teach";
import TeachStep2 from "./components/teacher/teach-step-2";
import RegisterTeacher from "./components/teacher/authTeacher/RegisterTeacher";
import LoginTeacher from "./components/teacher/authTeacher/LoginTeacher";

// Referal
import Referal from "./components/Refer/referal";
import Slider from "./components/Slider.js/Slider";
import Dashboard from "./components/Dashboard/Dashboard";
import TeacherCourses from "./components/Dashboard/TeacherCourses";
import communication from "./components/Dashboard/communication";
import performance from "./components/Dashboard/Performance";
function Wrapper() {
  return (
    <div className="home">
      <Header />
      {/* <Route path="/" component={Grid} />
      <Route path="/teach" component={Teach} /> */}
      <Route exact path="/" component={landing} />

      <Route exact path="/grid" component={Grid} />
      <Route exact path="/detail" component={CourseDetail} />
      <Route exact path="/enroll" component={Enroll} />

      <Footer />
    </div>
  );
}

export default Wrapper;
