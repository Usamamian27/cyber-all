import React from "react";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
// import Calc from "./components/Calc";
// import Header from "./components/layout/header";
// import Footer from "./components/layout/footer";
// import landing from "./components/layout/landing";
// import Grid from "./components/courses/grid";
// import CourseDetail from "./components/courses/courseDetail";
// import Enroll from "./components/courses/enroll";
// import TeachStep1 from "./components/teacher/teachStep1";
// // teacher
// import Teach from "./components/teacher/teach";
// import TeachStep2 from "./components/teacher/teach-step-2";
// import RegisterTeacher from "./components/teacher/authTeacher/RegisterTeacher";
// import LoginTeacher from "./components/teacher/authTeacher/LoginTeacher";

// // Referal
// import Referal from "./components/Refer/referal";
// import Slider from "./components/Slider.js/Slider";
// import Dashboard from "./components/Dashboard/Dashboard";
// import TeacherCourses from "./components/Dashboard/TeacherCourses";
// import communication from "./components/Dashboard/communication";
// import performance from "./components/Dashboard/Performance";

import Wrapper from "./Wrapper";
import WrapperTeacher from "./WrapperTeacher";
function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App wrapper">
          <Switch>
            <Route path="/teacher" component={WrapperTeacher} />
            <Route component={Wrapper} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
