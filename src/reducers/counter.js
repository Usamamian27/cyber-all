import { INCREMENT_VALUE, DECREMENT_VALUE } from "../utils/types";
const initialState = {
  myValue: 0
};

export default function(state = initialState, action) {
  switch (action.type) {
    case INCREMENT_VALUE:
      return {
        ...state,
        myValue: state.myValue + 1
      };
    case DECREMENT_VALUE:
      return {
        ...state,
        myValue: state.myValue - 1
      };
    default:
      return state;
  }
}
