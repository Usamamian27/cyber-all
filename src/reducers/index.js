import { combineReducers } from "redux";
import counter from "./counter";
// import authReducer from "./authReducer";
// import errorReducer from "./errorReducer";
// import categoryReducer from "./categoryReducer";
// import productReducer from "./productReducer";
// import addRoleReducer from "./addRoleReducer";
// import vendorReducer from "./authVendorReducer";
// import biddingReducer from "./biddingReducer";

// export default combineReducers({
//   auth: authReducer,
//   errors: errorReducer,
//   category: categoryReducer,
//   product: productReducer,
//   role: addRoleReducer,
//   authVendor: vendorReducer,
//   biddings: biddingReducer
// });
export default combineReducers({ counter: counter });
