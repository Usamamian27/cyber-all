import React from "react";
import { Link } from "react-router-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/layout/header";
import Footer from "./components/layout/footer";
import landing from "./components/layout/landing";
import Grid from "./components/courses/grid";
import CourseDetail from "./components/courses/courseDetail";
import Enroll from "./components/courses/enroll";
import TeachStep1 from "./components/teacher/teachStep1";
// teacher
import Teach from "./components/teacher/teach";
import TeachStep2 from "./components/teacher/teach-step-2";
import RegisterTeacher from "./components/teacher/authTeacher/RegisterTeacher";
import LoginTeacher from "./components/teacher/authTeacher/LoginTeacher";

// Referal
import Referal from "./components/Refer/referal";
import Slider from "./components/Slider.js/Slider";
import Dashboard from "./components/Dashboard/Dashboard";
import TeacherCourses from "./components/Dashboard/TeacherCourses";
import communication from "./components/Dashboard/communication";
import performance from "./components/Dashboard/Performance";
import Layout from "./components/Dashboard/Layout";
function WrapperTeacher() {
  return (
    <div className="home">
      <Switch>
        <Route exact path="/teacher" component={Teach} />
        <Route exact path="/teacher/teach-step-1" component={TeachStep1} />
        <Route exact path="/teacher/teach-step-2" component={TeachStep2} />
        <Route exact path="/teacher/register" component={RegisterTeacher} />
        <Route exact path="/teacher/login" component={LoginTeacher} />
        <Route exact path="/teacher/referal" component={Referal} />
        <Layout>
          <Route exact path="/teacher/dashboard" component={Dashboard} />
          <Route exact path="/teacher/add-courses" component={TeacherCourses} />
          <Route
            exact
            path="/teacher/communication"
            component={communication}
          />
          <Route exact path="/teacher/performance" component={performance} />
        </Layout>
        {/* <Route exact path="/teacher/layout" component={Layout} /> */}
      </Switch>
    </div>
  );
}

export default WrapperTeacher;
